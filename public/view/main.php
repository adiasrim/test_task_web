<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="../css/tailwind.min.css" rel="stylesheet">

    <title>Document</title>
</head>
<body>
    <div class="container mx-auto bg-black text-center mt-5">
        <h1 class="text-gray-400">hello world</h1>
    </div>
</body>
</html>