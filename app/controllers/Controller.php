<?php

namespace App\controllers;

use Core\BaseController;

class Controller extends BaseController
{
    public function index(): string
    {
        return self::view('main');
    }
}